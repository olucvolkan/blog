<?php
require_once 'db.php';


if (isset($_POST['submit'])){
    $title = $_POST['comment-name'];
    $mail = $_POST['comment-mail'];
    $description = $_POST['comments'];
    $post_id = $_GET['id'];
    $date =(new \DateTime())->format('Y-m-d H:i:s');

    $query = $db->prepare("INSERT INTO Comments SET title = ?,email = ?,description = ?,created_at = ?,post_id = ? ");
    $insert = $query->execute(array(
        $title,
        $mail,
        $description,
        $date,
        $post_id
    ));
    if ($insert) {
        header("Location:http://localhost:8000/untitled2/post.php?id=$post_id",true,301);
        exit();
    }
}
