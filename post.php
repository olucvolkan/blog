<?php require_once 'base.php';
$id = $_GET['id'];
$query = $db->query("SELECT * FROM Post WHERE id = '{$id}'")->fetch(PDO::FETCH_ASSOC);
$categoryId = $query['category_id'];
$postCategory = $db->query("SELECT * FROM Categories WHERE id = '{$categoryId}'")->fetch(PDO::FETCH_ASSOC);
$postComments = $db->query("SELECT * FROM Comments WHERE  post_id ={$query['id']}")->fetchAll(PDO::FETCH_ASSOC);
$totalComment = count($postComments);
?>


    <!-- Page Header -->
    <header class="masthead" style="background-image: url('img/post-bg.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="post-heading">
              <h1><?php  echo  $query['title']; ?></h1>
              <h2 class="subheading">
                  Kategori: <?php  echo  $postCategory['name']; ?>
              </h2>
              <span class="meta">Posted by
                <a href="#">Volkan</a>
                <?php  echo  $query['created_at']; ?></span>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Post Content -->
    <article>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
                <?php  echo  $query['detail'] ;?>
          </div>
        </div>
      </div>
    </article>
    <hr>



<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<div class="container">
    <?php foreach ($postComments as $postComment) { ?>
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            <div class="panel panel-white post panel-shadow">
                <div class="post-heading">
                    <div class="pull-left image">
                        <img src="http://bootdey.com/img/Content/user_1.jpg" class="img-circle avatar" alt="user profile image">
                    </div>
                    <div class="pull-left meta">
                        <div class="title h5">
                            <a href="#"><b><?php  echo  $postComment['title']?></b></a>
                            bir yorum yaptı
                        </div>
                        <h6 class="text-muted time"><?php  echo  $postComment['created_at']?></h6>
                    </div>
                    <br>

                </div>
                <div class="post-description">
                    <p> <?php  echo  $postComment['description']?></p>
                </div>
            </div>
        </div>
    </div>
    <?php  }?>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            <form class="form-horizontal" method="post" action="comment.php?id=<?php  echo  $query['id'];?>">
                <div class="form-group">
                    <label class="col-md-12 control-label" for="textarea">Adınız Soyadınız:</label>
                    <div class="col-md-12">
                        <input class="form-control" id="input" name="comment-name"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12 control-label" for="textarea">Mail Adres:</label>
                    <div class="col-md-12">
                        <input class="form-control" id="input" name="comment-mail"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12 control-label" for="textarea">Yorumunuz</label>
                    <div class="col-md-12">
                        <textarea class="form-control" id="textarea" name="comments"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <input  id="singlebutton" type="submit" name="submit" class="btn btn-primary"/>
                    </div>
                </div>
                <p>Toplam yorum sayısı: <?php  echo $totalComment ?></p>

        </div>
    </div>

</div>

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <ul class="list-inline text-center">
              <li class="list-inline-item">
                <a href="#">
                  <span class="fa-stack fa-lg">
                    <i class="fas fa-circle fa-stack-2x"></i>
                    <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <span class="fa-stack fa-lg">
                    <i class="fas fa-circle fa-stack-2x"></i>
                    <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <span class="fa-stack fa-lg">
                    <i class="fas fa-circle fa-stack-2x"></i>
                    <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li>
            </ul>
            <p class="copyright text-muted">Copyright &copy; Your Website 2018</p>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/clean-blog.min.js"></script>

  </body>

</html>
