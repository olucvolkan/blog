<?php
/**
 * Created by PhpStorm.
 * User: volki
 * Date: 28.10.2018
 * Time: 01:06
 */

require_once 'base.php';

$post_id = $_GET['id'];

$user_post = $db->query("SELECT * FROM post WHERE id='{$post_id}'")->fetch(PDO::FETCH_ASSOC);

$categories =  $db->query("SELECT * FROM Categories")->fetchAll(PDO::FETCH_ASSOC);

?>
    <div id="content-wrapper">

        <div class="container-fluid">

            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.php">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">Yazı Ekle</li>
            </ol>

            <!-- Page Content -->
            <form class="form-horizontal" method="post" action="post_edit.php?id=<?php echo $post_id; ?>">
                <fieldset>

                    <!-- Form Name -->
                    <legend>Post Edit</legend>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="textinput">Başlık</label>
                        <div class="col-md-8">
                            <input id="textinput" name="title" type="text" placeholder="başlık"
                                   class="form-control input-md" value="<?php echo $user_post['title']; ?>">

                        </div>
                    </div>
                    <!-- Textarea -->
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="textarea">Metin</label>
                        <div class="col-md-8">
                  <textarea class="ckeditor" name="post">
                                          <?php echo $user_post['detail']; ?>

                  </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="textarea">Tarih</label>
                        <div class="col-md-8">
                            <input type="date" class="form-control" name="post-date"/>
                            <?php echo  $user_post['created_at']; ?>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="textarea">Kategori Seç:</label>
                        <div class="col-md-8">
                            <select  title="Kategori" class="form-control" name="category">
                                <?php foreach ($categories as $category) {?>
                                    <option value="<?php echo $category['id'];?>"><?php echo $category['name']; ?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input id="singlebutton" type="submit" name="submit" class="btn btn-primary"/>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Your Website 2018</span>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    </body>

    </html>
<?php

if (isset($_POST['submit'])) {
    $title = $_POST['title'];
    $post = $_POST['post'];
    $date = $_POST['post-date'];
    $category = $_POST['category'];
    $query = $db->prepare("UPDATE post SET title = ?,detail = ?,created_at = ?,category_id = ? WHERE id = $post_id");
    $update = $query->execute(array(
        $title,
        $post,
        $date,
        $category
    ));
    header("Location:http://localhost:8000/untitled2/admin/posts.php", true, 301);
}
?>