<?php
/**
 * Created by PhpStorm.
 * User: volki
 * Date: 28.10.2018
 * Time: 00:25
 */

require_once 'base.php';
$posts = $db->query("SELECT * FROM post", PDO::FETCH_ASSOC);
?>


<!-- DataTables Example -->
<div class="container-fluid">
          <div class="card mb-12">
            <div class="card-header">
              <i class="fas fa-table"></i>
Data Table Example</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>İd</th>
                      <th>Title</th>
                      <th>Description</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php  foreach ($posts as $post) {?>
                    <tr>
                      <td><a href="#"><?php echo  $post['id'];?></a> </td>
                        <?php echo '<td><a href="post_edit.php?id='.$post['id'].'">' .$post['title'].'</a> </td>';?>
                      <td><?php echo substr( $post['detail'],0,50 ) ?></td>
                    </tr>
                  <?php  }?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>